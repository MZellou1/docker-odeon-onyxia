# docker-odeon

**WARNING** : utilise la version [multi GPU d'odeon](https://gitlab.com/StephanePEILLET/odeon-landcover/-/tree/lightning_multi_gpus/), toujours en développement.

Image **ign/odeon** contenant l'outil odeon pour l'entraînement de modèles d'IA 
de segmentation sémantiques sur des images aériennes et satellite.

S'appuie sur **ign/geoia**.

**WARNING : en cours de test, pas de garantie du bon fonctionnment, notamment des GPUs**

## Construction de l'image

docker build -f Dockerfile_short -t mzellou/ocsge \
    --build-arg PYTHON_VERSION=3.9 \
    --build-arg CUDA_VERSION=11.3 \
    .

    #--build-arg http_proxy="http://proxy.ign.fr:3128" \
    #--build-arg https_proxy="http://proxy.ign.fr:3128" \


Utiliser la variable de construction PYTHON_VERSION pour forcer la version de python.
(testé uniquement avec la 3.9)

```
make build
```

## Instanciation de conteneur

Pour utiliser odeon dans un environnement GPU, il est nécessaire d'avoir préparé l'hôte à l'exécution de docker dans un environnement GPU avec `nvidia-docker2`. Voir [documentation](http://dev-env.sai-experimental.ign.fr/outils/docker/installation-cuda/).

Lancement d'un entraînement en mode détaché avec accès aux données dans le dossier `/data` du container :

```
docker run --gpus all --ipc host --rm -d \
    -v /path/to/odeon/data:/data \
    ign/odeon:latest \
    odeon train -c config.json
```

