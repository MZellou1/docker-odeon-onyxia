#!/bin/bash

for i in $(ls test);
do
    echo "---------------------------------------------------------------------"
    echo "-- TEST $i...";
    echo "---------------------------------------------------------------------"
    cd test/$i
    docker build --no-cache -t $i . || { echo "*** FAILURE build $i"; exit 1; }
    docker run --rm $i || { echo "*** FAILURE run $i"; exit 1; }
    docker rmi -f $i
    echo "*** SUCCESS $i"
    cd ../..
done
