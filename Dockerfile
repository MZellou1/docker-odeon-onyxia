FROM inseefrlab/tensorflow

ARG PYTHON_VERSION
ENV PYTHON_VERSION=${PYTHON_VERSION}

# Dernière version Miniconda
ARG MINICONDA_URL="https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

# Mise à jour image et installation utilitaires
# RUN apt-get update && \
#     apt-get install -y --no-install-recommends wget && \
#     rm -rf /var/lib/apt/lists/*

# Installation Miniconda dans /opt/conda
ENV CONDA_DIR="/opt/conda"
RUN wget -q ${MINICONDA_URL} -O miniconda.sh && \
    /bin/bash miniconda.sh -b -p ${CONDA_DIR} && \
    rm miniconda.sh
ENV PATH=${CONDA_DIR}/bin:${PATH}
# Empêche la mise à jour silencieuse de conda
RUN conda config --set auto_update_conda False

# Installation python dans la version voulue
RUN conda install -y python=${PYTHON_VERSION}

# Installation mamba comme alternative à conda (plus rapide)
RUN conda install -y mamba -c conda-forge

########################################################################
## Docker GeoPython
## Image contenant les dépendances les plus courantes pour 
## le traitement des données géographiques avec python.
########################################################################

# Installation des libs de base pour la géomatique
RUN mamba install --yes \
    fiona \
    rasterio \
    geopandas \
    psycopg2 \
    geoalchemy2 \
    tqdm \
    -c conda-forge


########################################################################
## Docker Geo IA (pytorch)
## Image docker contenant les dépendances pour faire du 
## machine learning et du deep learning sur des données géographiques
########################################################################

# Force l'utilsation des versions GPU de pytorch, même si le build
# a lieu sur une machine CPU only.
ARG CUDA_VERSION
ENV CONDA_CUDA_OVERRIDE=${CUDA_VERSION}

# Installation des dépendances IA.
RUN mamba install --yes \
    pytorch=1.11.0 \
    torchvision>=0.5.0 \
    pytorch-lightning>=1.5.8 \
    torchmetrics>0.7.0 \
    cudatoolkit=${CUDA_VERSION} \
    scikit-learn>=0.23.1 \
    scikit-image>=0.16.2 \
    -c pytorch -c conda-forge

########################################################################
## Image odeon brute
########################################################################

ARG odeon_name='lightning_multi_gpus'
ENV ODEON_NAME=${odeon_name}

ARG odeon_url='https://gitlab.com/StephanePeillet/odeon-landcover'
ENV ODEON_URL=${odeon_url}

RUN git clone ${ODEON_URL}
#RUN cd /tf/odeon-landcover
WORKDIR odeon-landcover

RUN pwd 
RUN ls

RUN conda env create --file=environment.yml
RUN conda init bash
RUN exec "$SHELL" 

RUN pip install -e . \
    conda activate odeon \
    conda install -c anaconda ipykernel ipython_genutils -y -q \
    python -m ipykernel install --user --name=odeon

