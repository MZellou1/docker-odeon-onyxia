DOCKER_REGISTRY ?= qlf-docker-registry.ign.fr

ODEON_VERSION=latest
ODEON_BRANCH="lightning_multi_gpus"
ODEON_NAME="odeon-landcover-${ODEON_BRANCH}"
ODEON_URL="https://gitlab.com/StephanePEILLET/odeon-landcover/-/archive/${ODEON_BRANCH}/${ODEON_NAME}.tar.gz"

.PHONY: all build clean

all: build

build:
	docker build \
		--build-arg registry=${DOCKER_REGISTRY} \
		--build-arg ODEON_NAME=${ODEON_NAME} \
		--build-arg ODEON_URL=${ODEON_URL} \
		-t ign/odeon:${ODEON_VERSION} .

clean:
	docker image rm odeon:${ODEON_VERSION}
